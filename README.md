# TMD_COVID19

#### 介绍
这是一个关于新冠疫情的项目(其实是一个爬虫项目)
- 因为网上公开的疫情接口要不更新慢,要不就是速度慢(为了给自己的QQbot用); 
- 百度收录的地区数据大概都有
- 不知道后续会在添加些什么!
- 数据来源: 百度疫情实时大数据报告 
  - https://voice.baidu.com/act/newpneumonia/newpneumonia/?from=osari_aladin_banner

#### 软件架构
软件架构说明
- 使用了SpringBoot
- Mybatis
- mysql  
- jsoup
- fastjson-阿里巴巴
- RESTful风格
- slf4j做日志记录(酸辣粉4j哈哈)

![系统架构图](src/main/resources/static/imgs/TMD_COVID19.png "系统架构图")
#### 安装教程

1.  下载代码
2.  修改数据库地址,密码,建库,端口等配置
3.  直接运行或打包上传服务器运行

#### 使用说明

- 接口在controller包自行查看吧
- 现在有图片接口还有一个json的接口
- 还有记录接口调用次数的接口
- User的是以后可能会做一个web端(但一直没有动手)

#### 参与贡献

1.  暂时开发人员就是一个人,有问题的话联系邮箱
    - changhao_pan@qq.com

package top.drunkhao;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import top.drunkhao.entity.ApiLog;
import top.drunkhao.mapper.ApiLogMapper;

@Slf4j
@EnableScheduling
@SpringBootTest
class TmdCovid19ApplicationTests {

    @Autowired
    private ApiLogMapper apiLogMapper;
//    @Autowired
//    private ApiLog apiLog;
    @Test
    @Scheduled(cron = "1-5 * * * * ? ")
    void contextLoads() throws Exception {
        System.out.println("每5秒一次");
    }

    @Test
    void testLog() {
        String testInfo = "Free flying flowers are like dreams";
        log.info("The test info is :{}", testInfo);
    }
//    int findCountsByName(ApiLog apiLog);
//
//    void updateByApiNameSelective(ApiLog apiLog);

//    @Test
//    void testApi(){
//
//        ApiLog apiLog = new ApiLog();
//        apiLog.setApiName("cheshi");
//        int countsByName = apiLogMapper.findCountsByName(apiLog);
//        System.out.println(countsByName);
//        int counts = countsByName+1;
//        System.out.println(counts);
//        apiLog.setCounts(counts);
//        apiLogMapper.updateByApiNameSelective(apiLog);
//
//
//    }


}

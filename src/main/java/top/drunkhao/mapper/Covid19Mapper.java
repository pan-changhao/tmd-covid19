package top.drunkhao.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.drunkhao.entity.Covid19;


import java.util.ArrayList;
import java.util.List;

@Mapper
public interface Covid19Mapper {
    List<Covid19> findAll();

//    addCovid_19
    int addCovid_19(ArrayList<Covid19> covid19s);
//    batchUpdate
    int batchUpdate(ArrayList<Covid19> covid19s);
//    selectByPrimaryKey

    Covid19 selectByPrimaryKey(String city);

//    selectCountAll

    int selectCountAll();

//    truncateTable 清空表内所有数据
    void truncateTable();

}



package top.drunkhao.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.drunkhao.entity.ApiLog;


import java.util.List;

@Mapper
public interface ApiLogMapper {

//    findCountsByName
//    updateByApiNameSelective

    Integer findCountsByName(ApiLog apiLog);

    void updateByApiNameSelective(ApiLog apiLog);

//    findAll
    List<ApiLog> findAll();

}

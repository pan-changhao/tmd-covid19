package top.drunkhao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 2022-04-08
 * @author pch 
 * @ https://gitee.com/pan-changhao/tmd-covid19
 * @
 */

@MapperScan("top.drunkhao.mapper")//使用MapperScan批量扫描所有的Mapper接口；
@EnableScheduling
@SpringBootApplication
public class TmdCovid19Application {

    public static void main(String[] args) {
        SpringApplication.run(TmdCovid19Application.class, args);
    }

}

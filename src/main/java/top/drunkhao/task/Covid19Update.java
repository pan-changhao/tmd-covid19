package top.drunkhao.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import top.drunkhao.Tools.DateNow;
import top.drunkhao.mapper.Covid19Mapper;

import java.util.ArrayList;

import static top.drunkhao.Tools.Covid19_baidu.SpiderMan;
import static top.drunkhao.Tools.Covid19_world_baidu.WorldSpiderMan;

@Slf4j
@Component
public class Covid19Update {

    /**
     * 从第十秒开始每一分钟更新一次数据库内容
     */
    @Autowired
    public Covid19Mapper covid19Mapper;

    @Scheduled(cron = "0/59 0/1 * * * ?")
    public void Covid19UpdateData() throws Exception {

        int countAll = covid19Mapper.selectCountAll();



//        ArrayList arrayList = SpiderMan();
        ArrayList arrayList1 = WorldSpiderMan();
//        System.out.println(arrayList1);
//        int size = arrayList1.size() + arrayList.size();

        if (arrayList1.size() == 0){
            log.info("========================获取数据失败,等待下次获取=====================");
        }else if (arrayList1.size() == countAll ){
            int i = covid19Mapper.batchUpdate(arrayList1);
            log.info("========================数据量相同,进行批量更新=====================");
        }else if (arrayList1.size() > countAll){
            covid19Mapper.truncateTable();
            covid19Mapper.addCovid_19(arrayList1);
            log.info("==================疫情新增城市,清空表后重新批量插入=================");
        }else if (arrayList1.size() < countAll){
            log.info("===========国内或者国外数据处理异常,不予更新数据库=================");
        }

    }

}

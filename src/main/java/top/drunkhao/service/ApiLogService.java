package top.drunkhao.service;


import top.drunkhao.entity.ApiLog;

import java.util.List;

public interface ApiLogService {

    void ApiLogAdd(String apiName);
    Integer selectApi(String apiname);
    List<ApiLog> findAll();
}

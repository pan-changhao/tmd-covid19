package top.drunkhao.service;

import top.drunkhao.entity.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
}


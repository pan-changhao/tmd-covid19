package top.drunkhao.service;

import top.drunkhao.entity.Covid19;

import java.util.ArrayList;
import java.util.List;

public interface Covid19Service {
    List<Covid19> findAll();

    int addCovid_19() throws Exception;
    int batchUpdate() throws Exception;
    Covid19 selectByPrimaryKey(String city);

}

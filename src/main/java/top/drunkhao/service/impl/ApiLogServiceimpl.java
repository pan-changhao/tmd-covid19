package top.drunkhao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.drunkhao.entity.ApiLog;
import top.drunkhao.mapper.ApiLogMapper;
import top.drunkhao.service.ApiLogService;

import java.util.List;
@Service("apiLogService")
public class ApiLogServiceimpl implements ApiLogService {
    @Autowired
    private ApiLogMapper apiLogMapper;

    @Override
    public void ApiLogAdd(String apiName) {
        ApiLog apiLog = new ApiLog();
        apiLog.setApiName(apiName);
        int countsByName = apiLogMapper.findCountsByName(apiLog);
        int counts = countsByName+1;
        apiLog.setCounts(counts);
        apiLogMapper.updateByApiNameSelective(apiLog);
    }

    @Override
    public Integer selectApi(String apiname) {

        ApiLog apiLog = new ApiLog();
        apiLog.setApiName(apiname);
        int countsByName = apiLogMapper.findCountsByName(apiLog);

        return countsByName;
    }

    @Override
    public List<ApiLog> findAll() {
        return apiLogMapper.findAll();
    }
}

package top.drunkhao.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.drunkhao.entity.Covid19;
import top.drunkhao.mapper.Covid19Mapper;
import top.drunkhao.service.Covid19Service;

import java.util.ArrayList;
import java.util.List;

import static top.drunkhao.Tools.Covid19_baidu.SpiderMan;
import static top.drunkhao.Tools.Covid19_baidu.asciiToNative;

@Service("Covid19Service")
public class Covid19Serviceimpl implements Covid19Service {


    @Autowired
    public Covid19Mapper covid19Mapper;

    @Override
    public List<Covid19> findAll() {
        return covid19Mapper.findAll();
    }

    @Override
    public int addCovid_19() throws Exception {

        ArrayList arrayList = SpiderMan();
        int i = covid19Mapper.addCovid_19(arrayList);

        return i;
    }
//    city, curConfirm, asymptomatic, nativeRelative, confirmedRelative, asymptomaticRelative, confirmed, crued, died, updateTime
    @Override
    public int batchUpdate() throws Exception {

//        ArrayList arrayList = new ArrayList();
//        Covid19 c = new Covid19();
//        c.setCity("山东");
//        c.setCurConfirm("ttt");
//        c.setAsymptomatic("ttt");
//        c.setNativeRelative("ttt");
//        c.setConfirmedRelative("ttt");
//        c.setAsymptomaticRelative("ttt");
//        c.setConfirmed("ttt");
//        c.setDied("ttt");
//        c.setUpdateTime("ttt");
//        arrayList.add(c);
//        Covid19 c1 = new Covid19();
//        c1.setCity("上海");
//        c1.setCurConfirm("ttt");
//        c1.setAsymptomatic("ttt");
//        c1.setNativeRelative("ttt");
//        c1.setConfirmedRelative("ttt");
//        c1.setAsymptomaticRelative("ttt");
//        c1.setConfirmed("ttt");
//        c1.setDied("ttt");
//        c1.setUpdateTime("ttt");
//        arrayList.add(c1);
        ArrayList arrayList = SpiderMan();

        int i = covid19Mapper.batchUpdate(arrayList);


        return i;
    }

    @Override
    public Covid19 selectByPrimaryKey(String city) {


        return covid19Mapper.selectByPrimaryKey(city);
    }


}

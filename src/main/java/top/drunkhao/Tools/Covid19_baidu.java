package top.drunkhao.Tools;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import top.drunkhao.entity.Covid19;

import java.util.ArrayList;

/**
 * 爬取百度的数据类
 */
@Slf4j
public class Covid19_baidu {


//    public static void main(String[] args) throws Exception {
//        // 城市或者地区名：右上角的城市名/地区名
//        ArrayList arrayList = SpiderMan();
//        System.out.println(arrayList.size());
//        for (int i =0;arrayList.size() > i;i++){
//            System.out.println("==========="+i+"==========="+arrayList.get(i) );
//        }
//    }


    public static ArrayList SpiderMan() throws Exception {
        String cityUrl = "https://voice.baidu.com/act/newpneumonia/newpneumonia?city=上海-浦东新区";
        ArrayList covid19list = new ArrayList();
        Document doc = Jsoup.connect(cityUrl).get();
        // 获取第11个内联script标签内的内容
        Elements sEle = doc.select("script");
        String res = sEle.get(11).data();
        //将含有unicode字符串转成中文
        String s = asciiToNative(res);
        JSONObject jsonObject = JSONObject.parseObject(s);

        try {
            String s1 = jsonObject.toString();
            JSONObject jsonObject1 = JSONObject.parseObject(s1);
            Object component = jsonObject1.get("component");
            String s2 = component.toString();
            JSONArray jsonArray = JSONArray.parseArray(s2);
            Object o = jsonArray.get(0);
            String s3 = o.toString();
            JSONObject jsonObject2 = JSONObject.parseObject(s3);

            //            获得全国疫情数据
            Object caseList = jsonObject2.get("caseList");//全国疫情数据
            String s4 = caseList.toString();
            JSONArray jsonArray1 = JSONArray.parseArray(s4);


            for (int i = 0; jsonArray1.size() > i; i++) {
                Object o1 = jsonArray1.get(i);
                String s5 = o1.toString();
                JSONObject jsonObject3 = JSONObject.parseObject(s5);
                Covid19 covid192 = new Covid19();
                Object area = jsonObject3.get("area");
                covid192.setCity(area.toString());
                Object curConfirm = jsonObject3.get("curConfirm");
                covid192.setCurConfirm(curConfirm.toString());
                Object asymptomatic = jsonObject3.get("asymptomatic");
                covid192.setAsymptomatic(asymptomatic.toString());
                Object nativeRelative = jsonObject3.get("nativeRelative");
                covid192.setNativeRelative(nativeRelative.toString());
                Object confirmedRelative = jsonObject3.get("confirmedRelative");
                covid192.setConfirmedRelative(confirmedRelative.toString());
                Object asymptomaticRelative = jsonObject3.get("asymptomaticRelative");
                covid192.setAsymptomaticRelative(asymptomaticRelative.toString());
                Object confirmed = jsonObject3.get("confirmed");
                covid192.setConfirmed(confirmed.toString());
                Object crued = jsonObject3.get("crued");
                covid192.setCrued(crued.toString());
                Object died = jsonObject3.get("died");
                covid192.setDied(died.toString());
                Object updateTime = jsonObject3.get("updateTime");
                covid192.setUpdateTime(updateTime.toString());

                if (area.equals("境外输入") || area.equals("待确认")) {
//                System.out.println("境外输入与待确认不计入");
                } else {
                    covid19list.add(covid192);
                }


                Object subList = jsonObject3.get("subList");
                String s6 = subList.toString();
                JSONArray jsonArray2 = JSONArray.parseArray(s6);
                for (int j = 0; jsonArray2.size() > j; j++) {
                    Object o2 = jsonArray2.get(j);
                    String s7 = o2.toString();
                    JSONObject jsonObject4 = JSONObject.parseObject(s7);
                    Object city = jsonObject4.get("city");

                    Covid19 covid19 = new Covid19();
                    Object area1 = jsonObject4.get("city");
                    covid19.setCity(area1.toString());
                    Object curConfirm1 = jsonObject4.get("curConfirm");
                    covid19.setCurConfirm(curConfirm1.toString());
                    Object asymptomatic1 = jsonObject4.get("asymptomatic");
                    covid19.setAsymptomatic(asymptomatic1.toString());
                    Object nativeRelative1 = jsonObject4.get("nativeRelative");
                    covid19.setNativeRelative(nativeRelative1.toString());
                    Object confirmedRelative1 = jsonObject4.get("confirmedRelative");
                    covid19.setConfirmedRelative(confirmedRelative1.toString());
                    Object asymptomaticRelative1 = jsonObject4.get("asymptomaticRelative");
                    covid19.setAsymptomaticRelative(asymptomaticRelative1.toString());
                    Object confirmed1 = jsonObject4.get("confirmed");
                    covid19.setConfirmed(confirmed1.toString());
                    Object crued1 = jsonObject4.get("crued");
                    covid19.setCrued(crued1.toString());
                    Object died1 = jsonObject4.get("died");
                    covid19.setDied(died1.toString());
                    Object updateTime1 = jsonObject4.get("updateTime");
                    covid19.setUpdateTime(updateTime1.toString());
                    if (area1.equals("境外输入") || area1.equals("待确认")) {
//                    System.out.println("境外输入与待确认不计入");
                    } else {
                        covid19list.add(covid19);
                    }
                }
            }

        } catch (Exception e) {

//            System.out.println(e);
//            System.out.println("获取百度数据失败!!");
            log.info("===获取百度数据空指针,==国内疫情数据==大概是超时问题===异常类型:" + e);

        } finally {

            return covid19list;

        }


    }

    /**
     * 将含有unicode字符串转成中文
     *
     * @param asciicode
     * @return
     */
    public static String asciiToNative(String asciicode) {
        String[] asciis = asciicode.split("\\\\u");
        String nativeValue = asciis[0];
        try {
            for (int i = 1; i < asciis.length; i++) {
                String code = asciis[i];
                nativeValue += (char) Integer.parseInt(code.substring(0, 4), 16);
                if (code.length() > 4) {
                    nativeValue += code.substring(4, code.length());
                }
            }
        } catch (NumberFormatException e) {
            return asciicode;
        }
        return nativeValue;
    }


}

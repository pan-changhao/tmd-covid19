package top.drunkhao.Tools;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import top.drunkhao.entity.Covid19;

import java.util.ArrayList;
import java.util.Date;

import static top.drunkhao.Tools.Covid19_baidu.SpiderMan;

/**
 * 爬取百度的数据类  === 世界数据
 */
@Slf4j
public class Covid19_world_baidu {

//    public static void main(String[] args) throws Exception {
//        // 城市或者地区名：右上角的城市名/地区名
//        ArrayList arrayList = WorldSpiderMan();
//        System.out.println(arrayList.size());
//        for (int i = 0; arrayList.size() > i; i++) {
//            System.out.println("===========" + i + "===========" + arrayList.get(i));
//        }
//    }


    public static ArrayList WorldSpiderMan() throws Exception {
        String cityUrl = "https://voice.baidu.com/act/newpneumonia/newpneumonia?city=上海-浦东新区";

//        ArrayList covid19list = new ArrayList();
        ArrayList covid19list = SpiderMan();

        Document doc = Jsoup.connect(cityUrl).get();
        // 获取第11个内联script标签内的内容
        Elements sEle = doc.select("script");
        String res = sEle.get(11).data();
        //将含有unicode字符串转成中文
        String s = UnicodeToChinese.asciiToNative(res);
        JSONObject jsonObject = JSONObject.parseObject(s);

        try {

            String s1 = jsonObject.toString();
            JSONObject jsonObject1 = JSONObject.parseObject(s1);
            Object component = jsonObject1.get("component");
            String s2 = component.toString();
            JSONArray jsonArray = JSONArray.parseArray(s2);
            Object o = jsonArray.get(0);
            String s3 = o.toString();
            JSONObject jsonObject2 = JSONObject.parseObject(s3);

            Object globalList = jsonObject2.get("globalList");//世界数据
            String s8 = globalList.toString();
            JSONArray jsonArray1 = JSONArray.parseArray(s8);

            for (int i = 0; jsonArray1.size() > i; i++) {


                Object o1 = jsonArray1.get(i);
                String s5 = o1.toString();
                JSONObject jsonObject3 = JSONObject.parseObject(s5);

                String area = jsonObject3.get("area").toString();

                if (area.equals("热门")) {
                    continue;
                } else {
                    String curConfirm = jsonObject3.get("curConfirm").toString();
                    String confirmedRelative = jsonObject3.get("confirmedRelative").toString();
                    String confirmed = jsonObject3.get("confirmed").toString();
                    String crued = jsonObject3.get("crued").toString();
                    String died = jsonObject3.get("died").toString();
                    String updateTime = String.valueOf(new Date().getTime());
                    Covid19 covid192 = new Covid19(area, curConfirm, confirmedRelative, confirmed, crued, died, updateTime);
                    if (area.equals("境外输入") || area.equals("待确认")) {
//                System.out.println("境外输入与待确认不计入");
                    } else {
                        covid19list.add(covid192);
                    }
                    Object subList = jsonObject3.get("subList");
                    String s6 = subList.toString();
                    JSONArray jsonArray2 = JSONArray.parseArray(s6);
                    for (int j = 0; jsonArray2.size() > j; j++) {
                        Object o2 = jsonArray2.get(j);
                        String s7 = o2.toString();
                        JSONObject jsonObject4 = JSONObject.parseObject(s7);

                        String country = jsonObject4.get("country").toString();
                        String curConfirm2 = jsonObject4.get("curConfirm").toString();
                        String confirmedRelative2 = jsonObject4.get("confirmedRelative").toString();
                        String confirmed2 = jsonObject4.get("confirmed").toString();
                        String crued2 = jsonObject4.get("crued").toString();
                        String died2 = jsonObject4.get("died").toString();
                        String updateTime2 = String.valueOf(new Date().getTime());

                        Covid19 covid19 = new Covid19(country, curConfirm2, confirmedRelative2, confirmed2, crued2, died2, updateTime2);
                        if (country.equals("境外输入") || country.equals("待确认")) {
//                    System.out.println("境外输入与待确认不计入");
                        } else {
                            covid19list.add(covid19);
                        }
                    }
                }
            }

        } catch (Exception e) {

            log.info("===获取百度数据空指针,世界疫情数据==大概是超时问题===异常类型:" + e);

        } finally {

            return covid19list;

        }
    }
}

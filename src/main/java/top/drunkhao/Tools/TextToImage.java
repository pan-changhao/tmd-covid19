package top.drunkhao.Tools;

import java.awt.*;
import java.awt.image.BufferedImage;


public class TextToImage {


    public static BufferedImage createImage(String str) throws Exception {

        String[] strArr = str.split(",");

        Font font = new Font("微软雅黑", Font.BOLD, 40); // 字体大小
//        int image_height = 210; // 每张图片的高度
//        int line_height = 18; // 每行或者每个文字的高度
//        int width = 200;
        int image_height = strArr.length*55; // 每张图片的高度
        int line_height = 50; // 每行或者每个文字的高度
        int width = 600;
//        for (int i =0 ;strArr.length>i;i++){
//            System.out.println(strArr[i]);
//        }
        // 创建图片
        BufferedImage image = new BufferedImage(width,image_height,BufferedImage.TYPE_USHORT_565_RGB);
        Graphics2D g = image.createGraphics();
        g.setClip(0, 0, width, image_height);
        g.setColor(Color.white); // 背景色白色
        g.fillRect(0, 0, width, image_height);
        g.setColor(Color.red);//  字体颜色红色

        g.setFont(font);// 设置画笔字体
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB); // 抗锯齿
        // 画布

        g.drawString(strArr[0], 0, line_height);
//        g.setColor(Color.blue);//  字体颜色红色
        for (int i =1 ;strArr.length>i;i++){
            if (i==3 || i==4 || i==5){
                g.setFont(new Font("微软雅黑", Font.BOLD, 50));
                g.setColor(Color.red);//  字体颜色红色
                g.drawString(strArr[i], 0, (i+1)*line_height);
            }else {
                g.setColor(Color.blue);//  字体颜色红色

                g.setFont(new Font("微软雅黑", Font.BOLD, 30));
                g.drawString(strArr[i], 0, (i+1)*line_height);
            }

        }
//        g.drawString(strArr[0], 0, line_height);
        g.dispose(); // 释放资源
        return image;
    }
}

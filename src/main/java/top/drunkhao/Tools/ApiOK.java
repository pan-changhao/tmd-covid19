package top.drunkhao.Tools;



import org.springframework.web.client.RestTemplate;

/**
 * 专门用来访问第三方api的工具
 */

public class ApiOK {

    public static String doGet(String url) {
        RestTemplate dd =new RestTemplate();
        String forObject = dd.getForObject(url, String.class);
        return forObject;
    }


//    public static void main(String[] args) {
//        String cityUrl = "https://sp1.baidu.com/8aQDcjqpAAV3otqbppnN2DJv/api.php?query=111.15.203.117&co=&resource_id=5809&t=1649383842489";
//        doGet(cityUrl);
//    }

}

package top.drunkhao.Tools;



import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**

 获取访问的ip
 api监控的工具

 **/
public class IpUtil {
    private static final String LOCAL_IP = "127.0.0.1";

    public static String getIpAddr(HttpServletRequest request) {


        if (request == null) {
            return "unknown";
        }
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return "0:0:0:0:0:0:0:1".equals(ip) ? LOCAL_IP : ip;
    }

    /**
     * 得到ip所属地区
     * @param ip
     * @return
     */

    public static String getIpCity(String ip) throws IOException {

        String cityUrl = "https://sp1.baidu.com/8aQDcjqpAAV3otqbppnN2DJv/api.php?query="+ip+"&co=&resource_id=5809&t=1649383842489";
        String s = ApiOK.doGet(cityUrl);
        String s1 = UnicodeToChinese.asciiToNative(s);
        JSONObject jsonObject = JSONObject.parseObject(s1);
        Object data = jsonObject.get("data");
        String s2 = data.toString();
        JSONArray jsonArray = JSONArray.parseArray(s2);
        Object o = jsonArray.get(0);
        String s3 = o.toString();
        JSONObject jsonObject1 = JSONObject.parseObject(s3);
        Object location = jsonObject1.get("location");
        String s4 = location.toString();

        return s4;
    }

    public static void main(String[] args) throws IOException {
        String ipCity = getIpCity("112.15.203.118");
        System.out.println(ipCity);
    }

}



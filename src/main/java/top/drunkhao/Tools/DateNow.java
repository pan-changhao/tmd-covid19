package top.drunkhao.Tools;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateNow {



    public static String getDateNow() {
        //   获取当前日期
        Date now = new Date();
        //   通过指定格式实例化日期转化为字符串模板
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //   用模板将Date类型日期格式化成字符串
        String redate = dateFormat.format(now);

        return redate;
    }
}

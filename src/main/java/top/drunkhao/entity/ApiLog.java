package top.drunkhao.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiLog implements Serializable {

    //id
    int id;

    //调用的api名字
    String apiName;

    //调用次数
    int counts;
    public ApiLog() {
    }

    public ApiLog(int id, String apiName, int counts) {
        this.id = id;
        this.apiName = apiName;
        this.counts = counts;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    @Override
    public String toString() {
        return "ApiLog{" +
                "id=" + id +
                ", apiName='" + apiName + '\'' +
                ", counts=" + counts +
                '}';
    }
}

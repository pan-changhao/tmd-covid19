package top.drunkhao.entity;

import java.io.Serializable;

public class Covid19 implements Serializable {
    String city;
    //    -- 现有确诊
    String curConfirm;
    //-- 现有无症状
    String asymptomatic;
    //-- 新增本土确诊
    String nativeRelative;
    //-- 新增确诊
    String confirmedRelative;
    //-- 新增无症状
    String asymptomaticRelative;
    //-- 累计确诊
    String confirmed;
    //-- 累计治愈
    String crued;
    //-- 累计死亡
    String died;
    //-- 更新时间
    String updateTime;

    public Covid19() {
    }
/*
 *             "relativeTime":1649433600,       更新时间
 *             "country":"法国",                  地区名
 *             "confirmedRelative":"148768",    新增确诊
 *             "died":"143243",                 累计死亡
 *             "curConfirm":"2702561",          现有确诊
 *             "confirmed":"26839721",          累计确诊
 *             "crued":"23993917"               累计治愈
 */
    //世界疫情数据(area,curConfirm,                           confirmedRelative           ,confirmed,     crued,      died,       updateTime);
    public Covid19(String city, String curConfirm, String confirmedRelative,String confirmed, String crued, String died, String updateTime) {
        this.city = city;
        this.curConfirm = curConfirm;
        this.confirmedRelative = confirmedRelative;
        this.confirmed = confirmed;
        this.crued = crued;
        this.died = died;
        this.updateTime = updateTime;
    }
    public Covid19(String city, String curConfirm, String asymptomatic, String nativeRelative, String confirmedRelative, String asymptomaticRelative, String confirmed, String crued, String died, String updateTime) {
        this.city = city;
        this.curConfirm = curConfirm;
        this.asymptomatic = asymptomatic;
        this.nativeRelative = nativeRelative;
        this.confirmedRelative = confirmedRelative;
        this.asymptomaticRelative = asymptomaticRelative;
        this.confirmed = confirmed;
        this.crued = crued;
        this.died = died;
        this.updateTime = updateTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCurConfirm() {
        return curConfirm;
    }

    public void setCurConfirm(String curConfirm) {
        this.curConfirm = curConfirm;
    }

    public String getAsymptomatic() {
        return asymptomatic;
    }

    public void setAsymptomatic(String asymptomatic) {
        this.asymptomatic = asymptomatic;
    }

    public String getNativeRelative() {
        return nativeRelative;
    }

    public void setNativeRelative(String nativeRelative) {
        this.nativeRelative = nativeRelative;
    }

    public String getConfirmedRelative() {
        return confirmedRelative;
    }

    public void setConfirmedRelative(String confirmedRelative) {
        this.confirmedRelative = confirmedRelative;
    }

    public String getAsymptomaticRelative() {
        return asymptomaticRelative;
    }

    public void setAsymptomaticRelative(String asymptomaticRelative) {
        this.asymptomaticRelative = asymptomaticRelative;
    }

    public String getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }

    public String getCrued() {
        return crued;
    }

    public void setCrued(String crued) {
        this.crued = crued;
    }

    public String getDied() {
        return died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Covid19{" +
                "city='" + city + '\'' +
                ", curConfirm='" + curConfirm + '\'' +
                ", asymptomatic='" + asymptomatic + '\'' +
                ", nativeRelative='" + nativeRelative + '\'' +
                ", confirmedRelative='" + confirmedRelative + '\'' +
                ", asymptomaticRelative='" + asymptomaticRelative + '\'' +
                ", confirmed='" + confirmed + '\'' +
                ", crued='" + crued + '\'' +
                ", died='" + died + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}

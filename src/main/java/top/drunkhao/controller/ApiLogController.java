package top.drunkhao.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import top.drunkhao.Tools.IpUtil;
import top.drunkhao.entity.ApiLog;
import top.drunkhao.mapper.ApiLogMapper;
import top.drunkhao.service.ApiLogService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class ApiLogController {



    @Autowired
    private ApiLogService apiLogService;
    @Autowired
    private ApiLogMapper apiLogMapper;

    @RequestMapping("/selectApi/{name}")
    public int selectApi(@PathVariable("name") String name, HttpServletRequest request) throws IOException {

        apiLogService.ApiLogAdd("selectApi");
        //获取IP地址
        String ipAddress = IpUtil.getIpAddr(request);
        String header = request.getHeader("user-agent");
        String ipCity = IpUtil.getIpCity(ipAddress);
        log.info("===查询接口:"+name+"===来访IP==="+ipAddress+"===地区==="+ipCity+"===请求头==="+header);
        Integer apiLog = apiLogService.selectApi(name);


        return apiLog;
    }

    /**
     * 未完成==================================================
     * @return
     */


    @RequestMapping("/findAll")
    public List<ApiLog> findAll(){
        return apiLogService.findAll();
    }

}

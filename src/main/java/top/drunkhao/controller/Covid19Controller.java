package top.drunkhao.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.drunkhao.Tools.DateNow;
import top.drunkhao.Tools.IpUtil;
import top.drunkhao.Tools.TextToImage;
import top.drunkhao.entity.Covid19;
import top.drunkhao.service.ApiLogService;
import top.drunkhao.service.Covid19Service;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/convid19")
public class Covid19Controller {


    @Autowired
    private Covid19Service covid19Service;
    @Autowired
    private ApiLogService apiLogService;

    @RequestMapping("/findAll")
    public List<Covid19> findAll(HttpServletRequest request) throws IOException {

        //获取IP地址
        String ipAddress = IpUtil.getIpAddr(request);
        String header = request.getHeader("user-agent");
        String ipCity = IpUtil.getIpCity(ipAddress);
        log.info("===来访IP==="+ipAddress+"===地区==="+ipCity+"===请求头==="+header);


        return covid19Service.findAll();
    }

    @RequestMapping("/inster")
    public int inster() throws Exception {
        return covid19Service.addCovid_19();
    }
//    @RequestMapping("/batchUpdate")
//    public int batchUpdate() throws Exception {
//        return covid19Service.batchUpdate();
//    }

//    @RequestMapping("/selectByPrimaryKey/{city}")
//    public Covid19 selectByPrimaryKey(@PathVariable("city") String city){
//        return covid19Service.selectByPrimaryKey(city);
//    }

    /**
     * 对外开放接口
     * @param city
     * @return
     */
    @RequestMapping("/data/{city}")
    public Covid19 data(@PathVariable("city") String city,HttpServletRequest request) throws IOException {

        apiLogService.ApiLogAdd("data");

        //获取IP地址
        String ipAddress = IpUtil.getIpAddr(request);
        String header = request.getHeader("user-agent");
        String ipCity = IpUtil.getIpCity(ipAddress);
        log.info("===查询内容:"+city+"===来访IP==="+ipAddress+"===地区==="+ipCity+"===请求头==="+header);
        return covid19Service.selectByPrimaryKey(city);
    }

    /**
     * 一个返回图片的接口...
     * @param city
     * @param request
     * @param httpServletResponse
     * @throws Exception
     */
    @GetMapping("/imgs/{city}")
    public void generateQRCode(@PathVariable("city") String city,HttpServletRequest request,HttpServletResponse httpServletResponse) throws Exception {
        apiLogService.ApiLogAdd("imgs");
        //获取IP地址
        String ipAddress = IpUtil.getIpAddr(request);
        String header = request.getHeader("user-agent");
        String ipCity = IpUtil.getIpCity(ipAddress);
        log.info("===查询内容:"+city+"===来访IP==="+ipAddress+"===地区==="+ipCity+"===请求头==="+header);
        Covid19 covid19 = covid19Service.selectByPrimaryKey(city);



        String city1 = covid19.getCity();
        String curConfirm = covid19.getCurConfirm();
        String asymptomatic = covid19.getAsymptomatic();
        String nativeRelative = covid19.getNativeRelative();
        String confirmedRelative = covid19.getConfirmedRelative();
        String asymptomaticRelative = covid19.getAsymptomaticRelative();
        String confirmed = covid19.getConfirmed();
        String crued = covid19.getCrued();
        String died = covid19.getDied();
        String updateTime = covid19.getUpdateTime();
        String dateNow = DateNow.getDateNow();



        SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd hh:mm:ss");
        String sd2 = sdf2.format(new Date(Long.parseLong(String.valueOf(updateTime))*1000L));
        String data = " 地区  \t"+city1+"\n"+
                    ", 现有确诊  \t"+curConfirm+"\n"+
                    ", 现有无症状\t"+asymptomatic+"\n"+
                    ", 新增本土确诊\t"+nativeRelative+"\n"+
                    ", 新增确诊\t"+confirmedRelative+"\n"+
                    ", 新增无症状\t"+asymptomaticRelative+"\n"+
                    ", 累计确诊\t"+confirmed+"\n"+
                    ", 累计治愈\t"+crued+"\n"+
                    ", 累计死亡\t"+died+"\n"+
                    ", 更新时间:"+sd2+"\n"+
                    ", 现在时间"+dateNow;

        //返回BufferedImage的对象
        byte[] captchaOutputStream = null;
        ByteArrayOutputStream imgOutputStream = new ByteArrayOutputStream();


        BufferedImage challenge = TextToImage.createImage(data);
        ImageIO.write(challenge, "jpg", imgOutputStream);
        captchaOutputStream = imgOutputStream.toByteArray();
//        httpServletResponse.setHeader("Cache-Control", "no-store");
//        httpServletResponse.setHeader("Pragma", "no-cache");
//        httpServletResponse.setDateHeader("Expires", 0);
//        httpServletResponse.setContentType("image/jpeg");
        ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
        responseOutputStream.write(captchaOutputStream);
        responseOutputStream.flush();
        responseOutputStream.close();
    }





}

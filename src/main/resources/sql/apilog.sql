/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : clawler

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 12/04/2022 08:08:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for apilog
-- ----------------------------
DROP TABLE IF EXISTS `apilog`;
CREATE TABLE `apilog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'api名字',
  `counts` int(11) DEFAULT NULL COMMENT '调用计数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apilog
-- ----------------------------
INSERT INTO `apilog` VALUES (0, 'imgs', 35);
INSERT INTO `apilog` VALUES (2, 'cheshi2', 122);
INSERT INTO `apilog` VALUES (3, 'data', 84);
INSERT INTO `apilog` VALUES (4, 'selectApi', 0);

SET FOREIGN_KEY_CHECKS = 1;
